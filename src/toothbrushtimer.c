#include <pebble.h>

#define DEFAULT 0
#define TICKING 1
#define PAUSED 2
#define FINISHED 3

static Window *window;
static ActionBarLayer *actionbar;

static BitmapLayer *sanduhr;
static TextLayer *timer;

static AppTimer *timerHandle;

static GBitmap *container;
static GBitmap *bmp_actionbar_up;
static GBitmap *bmp_actionbar_select;
static GBitmap *bmp_actionbar_down;

static int currentMode = DEFAULT;
static int globalSeconds = 0;
static int brushtime = 180;
static int speed = 1000;

static void switch_to_mode(int mode);

/*
 * Helper
 */
static void deinit_container() {
	bitmap_layer_set_bitmap(sanduhr, NULL);
	if(container) {
		gbitmap_destroy(container);
	}
}

/**
 * ActionBar-Handling
 */
void set_actionbar_select_to_resource(int ressource_id) {
	action_bar_layer_clear_icon(actionbar, BUTTON_ID_UP);
	action_bar_layer_clear_icon(actionbar, BUTTON_ID_DOWN);
	gbitmap_destroy(bmp_actionbar_select);
	bmp_actionbar_select = gbitmap_create_with_resource(ressource_id);
	action_bar_layer_set_icon(actionbar, BUTTON_ID_SELECT, bmp_actionbar_select);
}
static void load_actionbar_bitmaps() {
	bmp_actionbar_up = gbitmap_create_with_resource(RESOURCE_ID_ACTIONBAR_UP);
	bmp_actionbar_select = gbitmap_create_with_resource(RESOURCE_ID_ACTIONBAR_START);
	bmp_actionbar_down = gbitmap_create_with_resource(RESOURCE_ID_ACTIONBAR_DOWN);
}
static void set_default_actionbar_icons() {
  load_actionbar_bitmaps();
  action_bar_layer_set_icon(actionbar, BUTTON_ID_UP, bmp_actionbar_up);
  action_bar_layer_set_icon(actionbar, BUTTON_ID_SELECT, bmp_actionbar_select);
  action_bar_layer_set_icon(actionbar, BUTTON_ID_DOWN, bmp_actionbar_down);
}

static void showSandglass(int resource_id) {
	deinit_container();
	container = gbitmap_create_with_resource(resource_id);
	bitmap_layer_set_bitmap(sanduhr, container);
}

static void update_seconds(int seconds) {
	static char time_text[] = "0:00";
	snprintf(time_text, sizeof(time_text), "%1d:%2.2d", seconds/60, seconds%60);
	text_layer_set_text(timer, time_text);
}

static void update_sandglass(int seconds) {
	// TODO: Make it scale with brushtime
	if(seconds > 132 && seconds <= 155) {
		showSandglass(RESOURCE_ID_SANDUHR_Z1);
	} else if(seconds > 107 && seconds <= 132) {
		showSandglass(RESOURCE_ID_SANDUHR_Z2);
	} else if(seconds > 82 && seconds <= 107) {
		showSandglass(RESOURCE_ID_SANDUHR_Z3);
	} else if(seconds > 57 && seconds <= 82) {
		showSandglass(RESOURCE_ID_SANDUHR_Z4);
	} else if(seconds > 31 && seconds <= 57) {
		showSandglass(RESOURCE_ID_SANDUHR_Z5);
	} else if(seconds <= 31) {
		showSandglass(RESOURCE_ID_SANDUHR_Z6);
	} else {
		showSandglass(RESOURCE_ID_SANDUHR_START);
	}
}

static int update_screen() {
	int seconds = brushtime - globalSeconds;
	update_seconds(seconds);
	update_sandglass(seconds);
	return seconds;
}

static void finishBrushing() {
	showSandglass(RESOURCE_ID_SANDUHR_ENDE);
	app_timer_cancel(timerHandle);
	static const uint32_t const segments[] = {400, 100, 800};
	VibePattern pat = {
		.durations = segments,
		.num_segments = ARRAY_LENGTH(segments),
	};
	vibes_enqueue_custom_pattern(pat);
}

static void handle_timer(void *data) {
	globalSeconds++;
	int seconds = update_screen();

	if(seconds > 0) {
		timerHandle = app_timer_register(speed, (AppTimerCallback)handle_timer, 0);
	} else {
		switch_to_mode(FINISHED);
	}
}

static void set_actionbar_icons() {
	switch(currentMode) {
		case DEFAULT:
			set_default_actionbar_icons();
			break;
		case TICKING:
			set_actionbar_select_to_resource(RESOURCE_ID_ACTIONBAR_PAUSE);
			break;
		case PAUSED:
			set_default_actionbar_icons();
			break;
		case FINISHED:
			set_actionbar_select_to_resource(RESOURCE_ID_ACTIONBAR_REDO);
			break;
	}
}

static void switch_to_mode(int mode) {
	currentMode = mode;
	switch(currentMode) {
		case DEFAULT:
			globalSeconds = 0;
			brushtime = 180;
			speed = 1000;
			showSandglass(RESOURCE_ID_SANDUHR_LEER);
			update_screen();
			break;
		case TICKING:
			timerHandle = app_timer_register(speed, (AppTimerCallback)handle_timer, 0);
			break;
		case PAUSED:
			speed = 1000;
			app_timer_cancel(timerHandle);
			break;
		case FINISHED:
			finishBrushing();
			break;
	}
	set_actionbar_icons();
}

static void select_click_handler(ClickRecognizerRef recognizer, void *context) {
	switch(currentMode) {
		case DEFAULT:
			switch_to_mode(TICKING);
			break;
		case TICKING:
			switch_to_mode(PAUSED);
			break;
		case PAUSED:
			switch_to_mode(TICKING);
			break;
		case FINISHED:
			switch_to_mode(DEFAULT);
			break;
	}
}

static void select_long_click_handler(ClickRecognizerRef recognizer, void *context) {
	switch(currentMode) {
		case TICKING:
			speed = 100;
			break;
	}
}

static void up_click_handler(ClickRecognizerRef recognizer, void *context) {
	switch(currentMode) {
		case DEFAULT:
		case PAUSED:
			brushtime += 30;
			if(brushtime > 570) {
				brushtime = 570;
			}
			update_screen();
			break;
	}
}

static void down_click_handler(ClickRecognizerRef recognizer, void *context) {
	switch(currentMode) {
		case DEFAULT:
		case PAUSED:
			if(brushtime > 30)
				brushtime -= 30;
			update_screen();
			break;
	}
}

static void click_config_provider(void *context) {
	window_single_click_subscribe(BUTTON_ID_SELECT, select_click_handler);
	window_single_click_subscribe(BUTTON_ID_UP, up_click_handler);
	window_single_click_subscribe(BUTTON_ID_DOWN, down_click_handler);
	window_long_click_subscribe(BUTTON_ID_SELECT, 0, select_long_click_handler, NULL);
}

static void window_load(Window *window) {
	Layer *window_layer = window_get_root_layer(window);

  	sanduhr = bitmap_layer_create(GRect(0, 10, 124, 77));
	bitmap_layer_set_alignment(sanduhr, GAlignCenter);
	bitmap_layer_set_background_color(sanduhr, GColorWhite);
	layer_add_child(window_layer, bitmap_layer_get_layer(sanduhr));

	timer = text_layer_create(GRect(0, 93, 124, 64));
	text_layer_set_text_alignment(timer, GTextAlignmentCenter);
	text_layer_set_font(timer, fonts_get_system_font(FONT_KEY_ROBOTO_BOLD_SUBSET_49));
	layer_add_child(window_layer, text_layer_get_layer(timer));

	// ActionBar
	actionbar = action_bar_layer_create();
	action_bar_layer_add_to_window(actionbar, window);
	action_bar_layer_set_click_config_provider(actionbar, click_config_provider);
	set_default_actionbar_icons();

	switch_to_mode(DEFAULT);
}

static void window_unload(Window *window) {
	action_bar_layer_destroy(actionbar);
	text_layer_destroy(timer);
	bitmap_layer_destroy(sanduhr);
}

static void init(void) {
  window = window_create();
  window_set_click_config_provider(window, click_config_provider);
  window_set_window_handlers(window, (WindowHandlers) {
    .load = window_load,
    .unload = window_unload,
  });
  const bool animated = true;
  window_stack_push(window, animated);
}

static void deinit(void) {
	deinit_container();
	window_destroy(window);
}

int main(void) {
  init();

  APP_LOG(APP_LOG_LEVEL_DEBUG, "Done initializing, pushed window: %p", window);

  app_event_loop();
  deinit();
}

