Toothbrushtimer
===============
Maintainer: Christoph Haefner
Current Version: 2.2

## Description
A Watch App to enhance the Pebble Smwartwatch.
Basically a digital version of the little sandglass I had when I was a kid to ensure I'm toothbrushing the proper amount of time.

## Build Instructions
 - Clone this repository in an appropriate directory via
	git clone http://gitlab.christophhaefner.de/pebble/toothbrushtimer.git
 - Build: pebble build
 - Install: pebble install

## Version history
 * 1   - Initial release
 * 1.1 - Added an ActionBar
 * 1.2 - Made toothbrushtime changeable by user
 * 1.3 - Doubled the amount of Sandglass-Animation-Frames & New animated Screenshot
 * 1.4
	* BugFix causing weird behaviour after pressing start-button twice
	* Two new ActionBar-Modes
 * 2.0 - Build with Pebble SDK 2.0
 * 2.0.1 - BugFix: Brushtime could get over 9:59 via Up-Clicks
 * 2.1 - Only changed the versionLabel, as Pebble now only allows Major.Minor
 * 2.2 - Fixed some minor issues:
	* Brushtime is now fixed to a 30s grid
	* Sandglass is not empty when for the first second after start
	* Build with latest Pebble SDK 3.0 (dp2) 

